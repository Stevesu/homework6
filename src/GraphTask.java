/**
 * Ylesanne: Koostada meetod etteantud graafi n astme leidmiseks (n on suurem vordne 0) kasutades korrutamist.
 * @author Steve
 */
public class GraphTask {

   /**
    * Main class. Calls subclasses
    * @param args as command-line arguments
     */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /**
    * Generate a graph and run subclasses.
    */
   public void run() {
      Graph g = new Graph ("G");

      g.createRandomSimpleGraph (10,9);
      g.findExponent(5);

   }

   /**
    * Class for vertex operations.
    */
   class Vertex {
      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;

      /**
       * Constructor for a vertex.
       * @param s as String. Id of a vertex.
       * @param v as next vertex.
       * @param e as first arc of the vertex.
        */
      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      /**
       * Contructor for a vertex.
       * @param s as String. Id of a vertex.
        */
      Vertex (String s) {
         this (s, null, null);
      }

       /**
        * Converts a vertex to a string.
        * @return id of a vertex as a string
        */
      @Override
      public String toString() {
         return id;
      }

      /**
       * Get the first arc of a vertex.
       * @return arc.
        */
      public Arc getFirst() {
         return first;
      }

       /**
        * Check if vertex has first arc set.
        * @return boolean. True if first vertex is set.
        */
      public boolean hasFirst() {
         return (getFirst() != null);
      }

      /**
       * Setter of the first arc for a vertex.
       * @param first as arc.
        */
      public void setFirst(Arc first) {
         this.first = first;
      }

      /**
       * Setter of the next vertex for a vertex.
       * @param next as vertex.
        */
      public void setNext(Vertex next) {
         this.next = next;
      }

   }

   /**
    * Class for arc operations
    */
   class Arc {
      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;

      /**
       * Arc constructor.
       * @param s as string. Id of the arc.
       * @param v as vertex. Target vertex of an arc.
       * @param a as arc. Next arc.
        */
      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      /**
       * Arc constructor
       * @param s as string. Id of the arc.
        */
      Arc (String s) {
         this (s, null, null);
      }

      /**
       * Convert an arc to a string.
       * @return id as string representation of an arc.
        */
      @Override
      public String toString() {
         return id;
      }

      /**
       * Set the next arc for an arc.
       * @param next as an arc.
        */
      public void setNext(Arc next) {
         this.next = next;
      }
   }

    /**
     * Class for graph operations
     */
   class Graph {
      private String id;
      private Vertex first;
      private int info = 0;

      /**
       * Graph constructor.
       * @param s as string. ID of the graph
       * @param v as vertex. First vertex of the graph.
        */
      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      /**
       * Graph constructor.
       * @param s as string. ID of the graph.
        */
      Graph (String s) {
         this (s, null);
      }

      /**
       * Converts an input to a string.
       * @return string format of the input.
        */
      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      /**
       * Create a vertex from a string.
       * @param vid as string input of the vertex id.
       * @return res as Vertex.
        */
      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      /**
       * Create an arc from a string.
       * @param aid as input string of the arc id.
       * @param from as vertex from which the current arc is from.
       * @param to as vertex to which the current arc is pointing.
        * @return res as an Arc.
        */
      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph.
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph.
       * @return adjacency matrix.
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple.
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices.
       * @param m number of edges.
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Converts a String format of a graph to matrix format.
       * @return matrix as integer matrix. Matrix representation of a graph.
        */
      public int[][] toMatrix() {
         String str = this.toString();
         int n = countVertex(str);
         String singleLines[]= separateLines(str, n);
         String vertexes[] = vertexStringArray(n);
         int matrix[][] = new int[n][n];

         for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
               if (singleLines[i].contains(vertexes[j]) && i!=j){
                  matrix[i][j] = 1;
               } else {
                  matrix[i][j]= 0;
               }
            }
         }
         return matrix;
      }

      /**
       * Create an array of all the vertex names of a graph.
       * @param n as the number of vertexes in a graph.
       * @return vertexes as String array of vertex names.
        */
      private String[] vertexStringArray(int n) {
         String vertexes[] = new String[n];
         for (int i = 0; i < n; i++) {
            int m = i+1;
            vertexes[i]= "v"+m;
         }
         return vertexes;
      }

      /**
       * Splits a string format of a graph to separate lines of vertexes.
       * @param str as string. String representation of a graph.
       * @param n as int. Number of vertexes.
       * @return separateLines as String array of vertexes with all the arcs.
        */
      private String[] separateLines(String str, int n) {
         String textStr[] = str.split("\\r?\\n");
         String separateLines[] = new String[n];
         int counter = 0;

         for (int i = 0; i < textStr.length; i++) {
            int l = textStr[i].indexOf(">");
            if (l!=-1) {
               separateLines[counter]=textStr[i];
               counter++;
            }
         }
         return separateLines;
      }

       /**
        * Counts the number of vertexes in the graph.
        * Method copied from http://stackoverflow.com/a/767910.
        * @param str as string. One for each vertex.
        * @return count as integer. Number of vertexes in the graph.
        */
      private int countVertex(String str) {
         String findStr = "-->";
         int lastIndex = 0;
         int count = 0;

         while(lastIndex != -1){
            lastIndex = str.indexOf(findStr,lastIndex);
            if(lastIndex != -1){
               count ++;
               lastIndex += findStr.length();
            }
         }
         return count;
      }

      /**
       * Calls methods to find the exponent n of the created graph and prints out the result.
       * @param n as integer. Exponent.
        */
      public void findExponent(int n) {
         if (n < 0){
            throw new RuntimeException("Viga sisendis. Aste peab olema >=0");
         } else if (n == 0) {
            System.out.println("Sisend graaf on:");
            System.out.println(this);
            System.out.println("Sisend graaf astmel " + n + " on:");
            System.out.println(1);
         } else {
            System.out.println("Sisend graaf on:");
            System.out.println(this);
            System.out.println("Sisend graaf astmel " + n + " on:");
            n = n-1;
            int m = countVertex(this.toString());
            int[][] x = this.toMatrix();
            int[][] y = x;

            for (int i = 0; i < n; i++) {
               y = multiplication(y, x, m);
            }
            y = graphMatrix(y, m);

            // Output graph in matrix format:
            /*
            for (int i = 0; i < m; i++) {
               for (int j = 0; j < m; j++) {
                  System.out.print(y[i][j]);
               }
               System.out.println();
            }
            */

            Graph result = new Graph ("G");
            result.createResultGraph(y, m);
            System.out.println(result);
         }
      }

       /**
        * Creates a graph from a matrix form.
        * @param y as int matrix. Matrix representation of a graph.
        * @param n as the number of vertexes in the graph.
        */
      private void createResultGraph(int[][] y, int n) {
         Vertex[] vert = vertexArray(n);
         first = vert[0];

         for (int i = 0; i < n; i++) {
            if (i < n-1) vert[i].setNext(vert[i+1]);
            int arcCount = 0;
            for (int j = 0; j < n; j++) {
               if (y[i][j]==1) arcCount++;
            }

            Arc[] arcs = new Arc[arcCount];
            int counter = 0;
            Vertex vi = vert [i];
            for (int j = 0; j < n; j++) {
               if (y[i][j] == 1) {
                  Vertex vj = vert[j];
                  arcs[counter] = new Arc("a" + vi.toString() + "_" + vj.toString(), vj, null);
                  counter++;
               }
            }

            if (!vert[i].hasFirst() && arcCount > 0) {
                  vert[i].setFirst(arcs[0]);
            }

            if (counter > 1) {
               for (int k = 0; k < counter - 1; k++) {
                  arcs[k].setNext(arcs[k + 1]);
               }
            }
         }
      }

      /**
       * Creates an array of all the vertexes of the graph.
       * @param n as int. Number of vertexes in the graph.
       * @return vertexes as Vertex array.
        */
      private Vertex[] vertexArray(int n) {
         Vertex vertexes[] = new Vertex[n];
         for (int i = 0; i < n; i++) {
            int m = i+1;
            vertexes[i]= new Vertex("v" + m);
         }
         return vertexes;
      }

      /**
       * Modify the matrix to indicate arcs of the graph.
       * @param y as the input int matrix.
       * @param n as number of rows and columns in the matrix.
       * @return y as output matrix of 1's and 0's, where 1 indicates that there is an arc.
        */
      private int[][] graphMatrix(int[][] y, int n) {
         for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
               if (i == j) {
                  y[i][j] = 0;
               } else if (y[i][j] > 0) {
                  y[i][j] = 1;
               }
            }
         } return y;
      }

      /**
       * Method for multiplying two same sized and with equal row and column number matrixes.
       * @param y as int matrix. First matrix to be multiplied.
       * @param x as int matrix. Second matrix to be multiplied.
       * @param n as int. Row and column number of matrixes.
        * @return z as int matrix. Result matrix of the multiplication.
        */
      private int[][] multiplication(int[][]y, int[][]x, int n) {
         int[][] z = new int[n][n];
         for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
               for (int k = 0; k < n; k++) {
                  z[i][j]+=y[i][k]*x[k][j];
               }
            }
         } return z;
      }
   }
}